

import { Component, ViewChild, OnInit, AfterViewInit } from '@angular/core';

// Import classes from maps module
import {
  GoogleMaps,
  GoogleMap,
  GoogleMapsEvent,
  LatLng,
  CameraPosition,
  MarkerOptions,
  GeocoderRequest,
  GeocoderResult,
  MyLocation,
  Marker,
  GoogleMapsAnimation,
  Geocoder,
  BaseArrayClass,
  ILatLng,
} from '@ionic-native/google-maps';

import { Platform, NavController, ToastController, LoadingController, } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { NativeGeocoder, NativeGeocoderOptions, NativeGeocoderResult } from '@ionic-native/native-geocoder/ngx';
import { OrderService } from 'src/app/services/order/order.service';
import { GlobalService } from 'src/app/services/global/global.service';

import { CallNumber } from '@ionic-native/call-number/ngx';

declare var google;

@Component({
  selector: 'app-maps-native',
  templateUrl: './maps-native.page.html',
  styleUrls: ['./maps-native.page.scss'],
})
export class MapsNativePage implements OnInit, AfterViewInit {
  titulo = 'Rastrea tu orden ';

  geoLatitude: number;
  geoLongitude: number;
  geoAccuracy: number;
  geoAddress: string;

  watchLocationUpdates: any;
  loading: any;
  isWatching: boolean;

  latitude: any;
  longitude: any;
  latLng: any = {};
  map: GoogleMap;
  address: string;
  geocoder: Geocoder;


  user: any = [];
  ordenes: any = [];
  orden: any = [];
  cadete: any = [];
  cadete_id: any;
  private skip = 0;
  mapRef = null;
  direckmark: any = [];
  kmText: any;
  timeArrive: any;

  directionsService: any = null;
  directionsDisplay: any = null;
  bounds: any = null;
  myLatLng: any;
  waypoints: any[];

  geoencoderOptions: NativeGeocoderOptions = {
    useLocale: true,
    maxResults: 5
  };
  cadeteLat: any;
  cadeteLng: any;

  constructor(
    public platform: Platform,
    public nav: NavController,
    private storage: Storage,
    private geolocation: Geolocation,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    public global: GlobalService,
    private callNumber: CallNumber



  ) {
    this.directionsService = new google.maps.DirectionsService();
    this.directionsDisplay = new google.maps.DirectionsRenderer();
    this.bounds = new google.maps.LatLngBounds();
  }

  ngOnInit() {
  }

  async ngAfterViewInit() {
    await this.platform.ready().then(() => {




      this.storage.get('orden').then((resp) => {
        this.orden = resp;
        console.log('Orden a rastrear:', this.orden);
        this.cadete_id = this.orden.repartidor_id;
        const direction = this.orden.direccion;
        console.log('direccion de la carga', direction);
        /* this.searchDirection(direction); */

        this.getGeoCodefromGoogleAPI(direction);

        this.global.getUbicacion(this.cadete_id).subscribe((resp) => {
          console.log(resp);
          this.cadete = resp;
          console.log(this.cadete);
          this.loadMap();
          /* this.goToMyLocation(); */
        });
      });

      /* Aqui corregir y utilizar positon de gogle maps */

      this.storage.get('position').then((resp) => {
        console.log('pocicion del localstorage', resp);
        this.latitude = resp.lat;
        this.longitude = resp.lng;
        const lat = resp.lat;
        const lng = resp.lng;
        const latLng = { lat, lng };
        /* alert(latLng); */

      });



    });
  }

  loadMap() {

    this.map = GoogleMaps.create('map', {
      camera: {
        target: {
          lat: -2.1683325,
          lng: -79.8348302
        },
        zoom: 18,
        tilt: 30
      }
    });

    this.map.one(GoogleMapsEvent.MAP_READY).then((data: any) => {

      const coordinates1: LatLng = new LatLng(this.cadete.lat, this.cadete.lng);
      const coordinates2: LatLng = new LatLng(this.orden.lat, this.orden.lng);

      const position = {
        target: coordinates1,
        zoom: 14
      };

      this.map.animateCamera(position);

      const markerOptions2: MarkerOptions = {
        position: coordinates2,
        icon: 'assets/images/man_3.png',
        title: this.orden.direccion
      };

      const marker2 = this.map.addMarker(markerOptions2)
        .then((marker2: Marker) => {
          marker2.showInfoWindow();
        });

      const markerOptions1: MarkerOptions = {
        position: coordinates1,
        icon: 'assets/images/scooter_1.png',
        title: this.cadete.name
      };

      const marker1 = this.map.addMarker(markerOptions1)
        .then((marker1: Marker) => {
          marker1.showInfoWindow();
        });


    });

  }

  backOrden() {
    this.storage.remove('orden');
  }

  actualizar() {

    this.map.clear();
    this.cadetePosition();
    console.log('actualizando');


    const coordinates1: LatLng = new LatLng(this.cadete.lat, this.cadete.lng);
    const coordinates2: LatLng = new LatLng(this.orden.lat, this.orden.lng);
    console.log('actualizando');
    const position = {
      target: coordinates1,
      zoom: 14
    };

    this.map.animateCamera(position);

    const markerOptions2: MarkerOptions = {
      position: coordinates2,
      icon: 'assets/images/man_3.png',
      title: this.orden.direccion
    };

    const marker2 = this.map.addMarker(markerOptions2)
      .then((marker2: Marker) => {
        marker2.showInfoWindow();
      });

    const markerOptions1: MarkerOptions = {
      position: coordinates1,
      icon: 'assets/images/scooter_1.png',
      title: this.cadete.name
    };

    const marker1 = this.map.addMarker(markerOptions1)
      .then((marker1: Marker) => {
        marker1.showInfoWindow();
      });



  }

  cadetePosition() {
    this.global.getUbicacion(this.cadete_id).subscribe((resp) => {
      this.cadete.lat = resp.lat;
      this.cadete.lng = resp.lng;
    });
  }

  llamarCadete() {
    this.callNumber.callNumber(this.cadete.telefono, true)
      .then(res => console.log('Llamando ', res))
      .catch(err => console.log('Ha ocurrido un error', err));
  }


  getGeoCodefromGoogleAPI(address: string) {
    this.global.getGeoCodefromGoogleAPI(address).subscribe((resp) => {
      console.log('resp googleApi', resp.results[0].geometry.location);
      this.direckmark = {
        lat: resp.results[0].geometry.location.lat,
        lng: resp.results[0].geometry.location.lng
      };
      console.log('direckMArk:', this.direckmark);

      this.calculateRoute();
    });
  }

  calculateRoute() {
    /* this.bounds.extend(this.myLatLng);
 
     this.map.fitBounds(this.bounds); */

    this.global.getUbicacion(this.cadete_id).subscribe((resp) => {

      /* console.log('Cadete desde calculate :', resp); */
      const cadtLat = resp.lat;
      const cadLng = resp.lng;

      /* console.log('latLng del cadete desde calculate:', cadtLat, cadLng);
      console.log('direckmark desde calculate:', this.direckmark.lat, this.direckmark.lng); */

      this.directionsService.route(
        {
          origin: new google.maps.LatLng(cadtLat, cadLng),
          destination: new google.maps.LatLng(this.orden.lat, this.orden.lng),
          travelMode: google.maps.TravelMode.DRIVING,
          avoidTolls: true
        },
        (response, status) => {
          if (status === google.maps.DirectionsStatus.OK) {
            console.log('respuesta de calculo de rutas', response);
            /* console.log('estoy buscando aqui:', response.routes[0].legs[0]);  */

            this.directionsDisplay.setDirections(response);
            this.kmText = response.routes[0].legs[0].distance.text;
            this.timeArrive = response.routes[0].legs[0].duration.text;
          } else {
            alert('Could not display directions due to: ' + status);
          }
        }
      );

    });
  }










}
