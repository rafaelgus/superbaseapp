import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Router, ActivatedRoute } from '@angular/router';
import { AlertService } from 'src/app/services/util/alert.service';
import { OrderService } from 'src/app/services/order/order.service';
import { LoadingController, AlertController, NavController } from '@ionic/angular';
import { PayPal, PayPalConfiguration, PayPalPayment } from '@ionic-native/paypal/ngx';
import { Storage } from '@ionic/storage';
import * as moment from 'moment';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { GlobalService } from 'src/app/services/global/global.service';

@Component({
  selector: 'app-order-history-details',
  templateUrl: './order-history-details.page.html',
  styleUrls: ['./order-history-details.page.scss'],
})
export class OrderHistoryDetailsPage implements OnInit {

  loading: any;
  user: any = [];
  ordenes: any = [];
  skip = 0;
  private SERVER = environment.server;
  orden: any = [];
  platillos: any = [];
  formasPago: any = [];
  selecion;
  idorder: any;
  orderRequestHour: any;
  orderCheckHour: any;
  canOrder;
  config: any = [];

  public repartidor_id: any;
  public whatsapp: string = '525584897183';
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private storage: Storage,
    private alertService: AlertService,
    private order: OrderService,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    private payPal: PayPal,
    private navCtrl: NavController,
    public global: GlobalService,
    private callNumber: CallNumber
  ) { }

  ngOnInit() {
    /* this.setBackButtonAction(); */
    this.mostrarProgressBar('Loading...');
    this.orden.pagoespecial = parseFloat(this.orden.pagoespecial);
    this.orden.total = parseFloat(this.orden.total);
    this.orden.cargo = parseFloat(this.orden.cargo);
    this.orden.iva = parseFloat(this.orden.iva);

    this.idorder = this.route.snapshot.paramMap.get('id');

    this.storage.get('user').then((resp) => {
      this.user = resp;

    });

    this.order.detalle_orden(this.idorder).subscribe((resp) => {
      console.log('orden:', resp);
      this.orden = resp;
      this.platillos = resp.platillos;
      this.orden.pagoespecial = parseFloat(this.orden.pagoespecial);
      this.orden.total = parseFloat(this.orden.total);
      this.orden.cargo = parseFloat(this.orden.cargo);
      this.orden.iva = parseFloat(this.orden.iva);
      this.orderRequestHour = resp.created_at;
      this.orderCheckHour = moment().format('dddd, MMMM Do YYYY, h:mm');

      const hourCancel = moment(this.orderRequestHour).add(5, 'minutes').format('dddd, MMMM Do YYYY, h:mm');



      console.log('tiempo de espera: ', hourCancel);

      this.checkOrderCancelTime();


      console.log(this.platillos);
      this.repartidor_id = resp.repartidor_id;

    });

    this.order.formasdepago().subscribe((resp) => {
      this.formasPago = resp;
    });
    if (this.orden.whatsapp !== null) {
      this.whatsapp = '34' + this.orden.whasapp;
    }

    this.global.getconfig().subscribe((resp) => {
      this.config = resp;

    });
  }

  checkOrderCancelTime() {

    const hourCancel = moment(this.orderRequestHour).add(5, 'minute').format('dddd, MMMM Do YYYY, h:mm');
    console.log('hora para cancelar funcion', hourCancel);
    console.log('hora de check funcion : ', this.orderCheckHour);

    if (hourCancel > this.orderCheckHour) {
      console.log('Si puede cancelar');
      this.canOrder = true;
    } else {
      console.log('no puede cancelar');
      this.canOrder = false;
    }
  }

  async mostrarProgressBar(message) {
    this.loading = await this.loadingCtrl.create({
      message,
      spinner: 'crescent',
      duration: 2000
    });
    return await this.loading.present();
  }
  cambiarEstado(id, estado) {
    this.mostrarProgressBar('Loading...');
    this.order.setEstadoOrden(id, estado);
  }

  cambiarPago(id) {
    this.mostrarProgressBar('Loading...');
    this.order
      .setPago({
        id,
        formaPago: this.selecion
      });
  }


  repetirOrden() {
    let platillos = {};
    for (let element of this.platillos) {
      platillos[element.pivot.platillos_id] = {
        id: element.pivot.platillos_id,
        cantidad: element.pivot.cantidad
      };
    }
    this.order
      .realizarOrden({
        usuario_id: this.user.id,
        direccion: this.orden.direccion,
        total: this.orden.total + 2,
        platillos
      })
      .subscribe(
        (resp) => {

          if (resp.sucess) {
            this.alertService.presentToast(
              'Su orden se ha recibido sera procesada y pronto le avisaremos cuando sea asignada a un repartidor!!!'
            );
            this.router.navigate(['/menu/landing']);
          } else {
            this.alertService.presentToast('Ha ocurrido un error');
          }
        },
        (error) => alert(this.user)
      );
  }

  backButton() {

    this.router.navigate(['/menu/orderhistory']);

  }
  cancelOrder() {
    this.alertService.presentToast('Por favor llama en los proximos 6 minnutos para proceder');
    this.callNumber.callNumber(this.config[4].valor, true)
      .then((res) => {
        console.log('Llamando ', res);
        this.router.navigate(['/menu/orderhistory']);
      })
      .catch(err => console.log('Ha ocurrido un error', err));

  }

  pagarOrden() {
    this.payPal
      .init({
        PayPalEnvironmentProduction: 'YOUR_PRODUCTION_CLIENT_ID',
        PayPalEnvironmentSandbox:
          'AaNFIZVtL2XB54QXJ4Ut0BTvQmRVG3N5kxAC2S2xbttz0pGrsyxdpwR3ULSELFIcShVtHDEeUY0f7Tm9'
      })
      .then(
        () => {
          this.payPal.prepareToRender('PayPalEnvironmentSandbox', new PayPalConfiguration({})).then(
            () => {
              let payment = new PayPalPayment(this.orden.total, 'USD', 'Description', 'sale');
              this.payPal.renderSinglePaymentUI(payment).then(
                (resp) => {
                  if (resp.response.state == 'approved')
                    this.alertService.presentToast('tu pago ha sido aprovado');
                  else this.alertService.presentToast('tu pago no pudo ser procesado');
                },
                () => { }
              );
            },
            () => { }
          );
        },
        () => { }
      );
  }

	/*  setBackButtonAction() {
  this.navBar.backButtonClick = () => {
    //Write here wherever you wanna do
    if (this.user.rol == 3) {
      this.navCtrl.pop();
    } else {
      this.navCtrl.setRoot('HistorialPedidoPage');
    }
  };
} */

	/* verMapa() {
  let alert = this.alertCtrl.create({
    header: 'Request permission',
    message: 'Ist necessary share your location!!',
    buttons: [
      {
        text: 'Cancel',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      },
      {
        text: 'Allow',
        handler: () => {
          if (this.user.rol == 2) {
            console.log('envio id ' + this.repartidor_id);
            this.navCtrl.push(HomePage, {
              rol: 2,
              id: this.repartidor_id
            });
          } else {
            console.log('envio direccion ' + this.orden.direccion);
            this.navCtrl.push(HomePage, {
              rol: 3,
              direccion: this.orden.direccion
            });
          }
        }
      }
    ]
  });
  alert.present();
} */

  verMapa(orden) {
    this.router.navigate(['ruta-orden/:id', { id: orden.id }]);
    this.storage.set('orden', orden);
  }

}
