import { Component, OnInit, NgZone, AfterContentInit, AfterViewInit, AfterContentChecked } from '@angular/core';
import { ModalController, AlertController, Platform, LoadingController } from '@ionic/angular';
import { Router, ActivatedRoute, NavigationStart } from '@angular/router';
import { Storage } from '@ionic/storage';
import { AlertService } from 'src/app/services/util/alert.service';
import { GlobalService } from 'src/app/services/global/global.service';
import { OrderService } from 'src/app/services/order/order.service';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { NativeGeocoder, NativeGeocoderOptions, NativeGeocoderResult } from '@ionic-native/native-geocoder/ngx';
import { PayPal, PayPalPayment, PayPalConfiguration } from '@ionic-native/paypal/ngx';
import { environment } from 'src/environments/environment';
import { ModalFacturaPage } from 'src/app/pages/global/modal-factura/modal-factura.page';
import { ModalPaypalPage } from 'src/app/pages/global/modal-paypal/modal-paypal.page';
import { PaypalCheckboxPage } from 'src/app/pages/global/paypal-checkbox/paypal-checkbox.page';
import { BrowserStack } from 'protractor/built/driverProviders';
import { HttpHeaderResponse } from '@angular/common/http';
import {
  GoogleMaps,
  GoogleMap,
  GoogleMapsEvent,
  LatLng,
  CameraPosition,
  MarkerOptions,
  GeocoderRequest,
  GeocoderResult,
  MyLocation,
  Marker,
  GoogleMapsAnimation,
  Geocoder,
  LocationService,
} from '@ionic-native/google-maps';


declare var google;

@Component({
  selector: 'app-request-data',
  templateUrl: './request-data.page.html',
  styleUrls: ['./request-data.page.scss'],
})
export class RequestDataPage implements OnInit, AfterContentInit, AfterViewInit, AfterContentChecked {

  mapnat: GoogleMap;
  address: string;
  geocoder: Geocoder;

  SERVER: string = environment.server;
  sucursales: any = [];
  sucursal: any = [];
  sucdisteval = [];
  sucursalLat: any;
  sucursalLng: any;
  sucursalId: any;
  distancia: any;
  sucursalOrden: any = [];
  maximo = 0;
  private loading: any;
  user: any = [];
  /* private direccion: any; */
  private listPedido: any = [];
  private data: any = [];
  formasPago;
  selecion;
  private seleccion;
  private subTotal: string;
  subTotal2: number;
  private iva = 12;
  private cargoPaypal = 6.6;
  private total: number;
  cargoDomicilio = 0;
  observaciones: string;
  factura: any;
  GoogleAutocomplete = new google.maps.places.AutocompleteService();
  autocomplete = { input: '' };
  autocompleteItems = [];
  dir = '';
  private paypaldata: any = [];

  directionsService: any = null;
  directionsDisplay: any = null;
  bounds: any = null;
  myLatLng: any;
  map: any;
  direckmark: any = [];
  kmText: any;
  timeArrive: any;
  distanceMatrix: any;
  sucursalLatobj: any;
  sucursalLngobj: any;
  mapdir;
  totalPaypalFactura: any;
  totalPaypalNoFactura: any;

  userLat;
  userLng;

  navigationSubscription;

  constructor(
    private modalCtrl: ModalController,
    private router: Router,
    private route: ActivatedRoute,
    private storage: Storage,
    private alertService: AlertService,
    private global: GlobalService,
    private order: OrderService,
    private geolocation: Geolocation,
    private nativeGeocoder: NativeGeocoder,
    public zone: NgZone,
    private alert: AlertController,
    private payPal: PayPal,
    private platform: Platform,
    public loadingController: LoadingController


  ) {
    this.geocoder = new google.maps.Geocoder();
    this.directionsService = new google.maps.DirectionsService();
    this.directionsDisplay = new google.maps.DirectionsRenderer();

    this.distanceMatrix = new google.maps.DistanceMatrixService();
    this.bounds = new google.maps.LatLngBounds();
    this.GoogleAutocomplete = new google.maps.places.AutocompleteService();




    /* this.navigationSubscription = this.router.events.subscribe((e: any) => {
      // If it is a NavigationEnd event re-initalise the component
      if (e instanceof NavigationStart) {
        this.startLocation();
      }
    }); */
  }

  /*  initialiseInvites() {
     console.log('hola desde aqui');
     this.startLocation();
   } */

  ngOnInit() {
    this.platform.ready();
    this.pruebaNativeGeocode();
    this.factura = false;
    this.selecion = 1;

    this.storage.get('mapsLocation').then((resp) => {
      console.log(resp);
      const lat = resp.lat;
      const lng = resp.lng;

      this.userLat = resp.lat;
      this.userLng = resp.lng;
      this.storage.set('userOrderLat', lat);
      this.storage.set('userOrderLng', lng);

      this.global.getconfig().subscribe((conf) => {
        console.log('configuracion', conf);
        const lat2 = conf[2].valor;
        const lng2 = conf[3].valor;

        console.log('latitudes:', lat, lng, lat2, lng2);

        this.calcularTaxa(lat, lng, lat2, lng2);
      });



    });

    this.order.formasdepago().subscribe((resp) => {
      this.formasPago = resp;
      console.log('formas de pago:', this.formasPago);
    });
    this.storage.get('subtotal').then((resp) => {
      /* this.subTotal = this.route.snapshot.paramMap.get('subTotal'); */
      this.subTotal = resp;
      this.subTotal2 = +this.subTotal;
    });

    this.storage.get('pedido').then((resp) => {
      this.listPedido = resp;
    });

    this.storage.get('user').then((val) => {
      this.user = val;
    });

    this.getSucursales();


  }



  pruebaNativeGeocode() {
    LocationService.getMyLocation().then((location: MyLocation) => {
      console.log(JSON.stringify(location, null, 2));
      const latLng = location.latLng;

      // Latitude, longitude -> address
      Geocoder.geocode({
        position: latLng
      }).then((results: GeocoderResult[]) => {
        if (results.length === 0) {
          // Not found
          return null;
        }

        const address: any = [
          results[0].extra.lines[0] || '',
          results[0].extra.lines[1] || '',
          results[0].extra.lines[2] || '',
          results[0].extra.lines[3] || '',
          results[0].extra.lines[4] || '',
          results[0].extra.lines[5] || '',
          results[0].extra.lines[6] || '',
        ].join(' ');



        console.log(address);
        console.log(results);

        this.storage.set('mapsAddress', address);
        this.mapdir = address;
        this.autocomplete = { input: address };
        this.userLat = latLng.lat;
        this.userLng = latLng.lng;
        /* alert(this.mapdir);
        alert(this.userLat);
        alert(this.userLng);
        alert([this.userLat, this.userLng]); */

      });


    });
  }



  ngAfterContentInit() {



  }
  ngAfterViewInit() {

  }

  ngAfterContentChecked() {

  }

  async startLocation() {
    alert('localizacion activa');

    this.geolocation
      .getCurrentPosition()
      .then((response) => {
        console.log(response);
        alert(response);

        /*  this.getDireccion(response.coords.latitude, response.coords.longitude); */
        /* this.calcularTaxa(response.coords.latitude, response.coords.longitude); */
      })
      .catch((error) => {
        this.alertService.presentToast('No tiene activo su GPS');
      });



  }


  ionViewWillLeave() {

  }



  getDireccion(lat, lng) {
    this.nativeGeocoder
      .reverseGeocode(lat, lng)
      .then((result: NativeGeocoderResult[]) => {
        console.log(result);
        const address = [
          (result[0].thoroughfare || '') + ' ' + (result[0].subThoroughfare || ''),
          result[0].locality
        ].join(', ');

        this.autocomplete.input = address;
        alert(address);
      })
      .catch((error: any) => { });
  }

  updateSearchResults() {
    if (this.autocomplete.input === '') {
      this.autocompleteItems = [];
      return;
    }
    this.GoogleAutocomplete.getPlacePredictions({ input: this.autocomplete.input }, (predictions, status) => {
      this.autocompleteItems = [];
      this.zone.run(() => {
        predictions.forEach((prediction) => {
          this.autocompleteItems.push(prediction);
        });
      });
    });
  }

  selectSearchResult(item) {
    console.log(item);
    this.storage.set('dir', item.place_id);
    this.autocomplete.input = item.description;
    this.autocompleteItems = [];
    this.dir = item.place_id;
    this.getSucursales();
    this.loadingUpdate();
    this.calcularTaxaString(item.place_id);
  }

  async loadingUpdate() {
    const loading = await this.loadingController.create({
      spinner: 'circles',
      duration: 5000,
      message: 'Por favor espere ...',
      translucent: true,

    });
    return await loading.present();
  }


  /*  distanceMatrixFunc() {
     const origin = 'Aeroporto da Madeira';
     const destinations = 'Hotel Four Views Baía, Rua das Maravilhas, Funchal';
     this.distanceMatrix.getDistanceMatrix(
       {
         origins: [origin],
         destinations: [destinations],
         travelMode: google.maps.TravelMode.DRIVING,
         unitSystem: google.maps.UnitSystem.METRIC,
         avoidHighways: false,
         avoidTolls: true
       },
       (response, status) => {
         console.log('distance matrix: ', response);
       },
       (error) => {
         console.log(error);
       }
     );
   } */

  async abrirFactura() {
    if (this.factura === false) {
      const modal = await this.modalCtrl.create({
        component: ModalFacturaPage,
        componentProps: {},
        cssClass: 'my-custom-modal-css',
        backdropDismiss: false
      });

      await modal.present();
      const { data } = await modal.onDidDismiss();
      this.data = data;
      console.log(this.data);
    }
  }
  async abrirPaypal() {
    const modal = await this.modalCtrl.create({
      component: ModalPaypalPage,
      componentProps: {},
      cssClass: 'my-custom-modal-css',
      backdropDismiss: false
    });

    await modal.present();
    const { data } = await modal.onDidDismiss();
    this.paypaldata = data;
    console.log(this.paypaldata);
  }
  async abrirPaypalBox() {
    const modal = await this.modalCtrl.create({
      component: PaypalCheckboxPage,
      componentProps: {},
      cssClass: 'my-custom-modal-css',
      backdropDismiss: true
    });

    await modal.present();
    const { data } = await modal.onDidDismiss();
    this.paypaldata = data;
    console.log(this.paypaldata);
  }

  getConfig() {

    this.global.getconfig().subscribe((conf) => {
      console.log('configuracion', conf);
      this.sucursalLatobj = conf[2].valor;
      this.sucursalLngobj = conf[3].valor;

      console.log('sucursal lat objet:', this.sucursalLatobj, 'sucursal long objet:', this.sucursalLngobj);
    });



  }

  getSucursales() {
    this.global.getSucursales().subscribe((resp) => {

      this.sucursales = resp;
      console.log('las sucursales:', this.sucursales);

      for (let item of this.sucursales) {
        this.sucursalLat = item.lat;
        this.sucursalLng = item.lng;
        this.sucursalId = item.id;
        /* console.log('latitud:', this.sucursalLat, 'longitud:', this.sucursalLng); */
        this.calcularDistancia(this.sucursalLat, this.sucursalLng, this.sucursalId);
      }
    });
    /* this.sucursalOrden = this.sucursal.reduce(function (prev, current) {
      return prev.distancia < current.distancia ? prev : current;
    }); //returns object */

    /* 	console.log(this.sucursalOrden); */
  }
  calcularDistancia(slat, slng, sid) {
    this.storage.get('dir').then(
      (resp) => {
        const place = resp;
        this.directionsService.route(
          {
            origin: {
              placeId: place
            },
            destination: new google.maps.LatLng(slat, slng),

            travelMode: google.maps.TravelMode.DRIVING,
            avoidTolls: true
          },
          (response, status) => {
            if (status === google.maps.DirectionsStatus.OK) {
              this.directionsDisplay.setDirections(response);
              const km = response.routes[0].legs[0].distance.value / 1000;

              this.sucursal.push({ id: sid, distancia: km });
              this.sucdisteval.push(km);
              this.sucursalOrd(this.sucursal);
              //returns object
            } else {
              alert('Could not display directions due to: ' + status);
            }
          },
          (error) => {
            console.log(error);
            alert(JSON.stringify(error));
          }
        );
      },
      (error) => {
        console.log(error);
        alert(JSON.stringify(error));
      }
    );
  }

  sucursalOrd(sucursal) {

    this.sucursalOrden = sucursal.reduce((prev, current) => {
      return prev.distancia < current.distancia ? prev : current;
    });
    /* console.log('La sucursal de la orden:', this.sucursalOrden) */

  }

  calcularTaxa(lat, lng, lat2, lng2) {
    this.directionsService.route(
      {
        origin: new google.maps.LatLng(lat, lng),
        destination: new google.maps.LatLng(lat2, lng2),

        travelMode: google.maps.TravelMode.DRIVING,
        avoidTolls: true
      },
      (response, status) => {
        if (status === google.maps.DirectionsStatus.OK) {
          this.directionsDisplay.setDirections(response);
          const km = response.routes[0].legs[0].distance.value / 1000;

          if (km >= 0 && km <= 7) {
            this.cargoDomicilio = 4;
          } else if (km >= 8 && km <= 8.99999) {
            this.cargoDomicilio = 4.5;
          } else if (km >= 9 && km <= 9.99999) {
            this.cargoDomicilio = 5;
          } else if (km >= 10 && km <= 10.99999) {
            this.cargoDomicilio = 5.5;
          } else if (km >= 11 && km <= 11.99999) {
            this.cargoDomicilio = 6;
          } else if (km >= 12 && km <= 12.99999) {
            this.cargoDomicilio = 6.5;
          } else if (km >= 13 && km <= 13.99999) {
            this.cargoDomicilio = 7;
          } else if (km >= 14 && km <= 14.99999) {
            this.cargoDomicilio = 7.5;
          } else if (km >= 15 && km <= 15.99999) {
            this.cargoDomicilio = 8;
          } else if (km >= 16 && km <= 16.99999) {
            this.cargoDomicilio = 8.5;
          } else if (km >= 17 && km <= 17.99999) {
            this.cargoDomicilio = 9;
          } else if (km >= 18 && km <= 18.99999) {
            this.cargoDomicilio = 9.5;
          } else if (km >= 19 && km <= 19.99999) {
            this.cargoDomicilio = 10;
          } else if (km >= 20 && km <= 20.99999) {
            this.cargoDomicilio = 10.5;
          } else if (km >= 21 && km <= 21.99999) {
            this.cargoDomicilio = 11;
          } else if (km >= 22 && km <= 22.99999) {
            this.cargoDomicilio = 11.5;
          } else if (km >= 23 && km <= 23.99999) {
            this.cargoDomicilio = 12;
          } else if (km >= 24 && km <= 24.99999) {
            this.cargoDomicilio = 12.5;
          } else if (km >= 25 && km <= 25.99999) {
            this.cargoDomicilio = 13;
          } else if (km >= 26 && km <= 26.99999) {
            this.cargoDomicilio = 13.5;
          } else if (km >= 27 && km <= 27.99999) {
            this.cargoDomicilio = 14;
          } else if (km >= 28 && km <= 28.99999) {
            this.cargoDomicilio = 14.5;
          } else if (km >= 29 && km <= 29.99999) {
            this.cargoDomicilio = 15;
          } else if (km >= 30 && km <= 30.99999) {
            this.cargoDomicilio = 15.5;
          } else if (km >= 31 && km <= 31.99999) {
            this.cargoDomicilio = 16;
          } else if (km >= 32 && km <= 32.99999) {
            this.cargoDomicilio = 16.5;
          } else if (km >= 33 && km <= 33.99999) {
            this.cargoDomicilio = 17;
          } else if (km >= 34 && km <= 34.99999) {
            this.cargoDomicilio = 17.5;
          } else if (km >= 35 && km <= 35.99999) {
            this.cargoDomicilio = 18;
          } else if (km >= 36) {
            console.log('no aplica');
            this.cargoDomicilio = 0;
            this.alertService.presentToast(
              'La distancia de tu ubicacion  no esta contemplada en nuestro rango de servicio'
            );
          }
        } else {
          alert('Could not display directions due to: ' + status);
        }
      },
      (error) => {
        console.log(error);
        alert(JSON.stringify(error));
      }
    );
  }

  calcularTaxaString(dir) {
    this.storage.get('dir').then((resp) => {
      const place1 = resp;
      this.global.getconfig().subscribe((conf) => {

        const lat2 = conf[2].valor;
        const lng2 = conf[3].valor;

        this.sucursalLatobj = lat2;
        this.sucursalLngobj = lng2;

      });

      this.directionsService.route(
        {
          origin: {
            placeId: dir
          },
          destination: new google.maps.LatLng(this.sucursalLatobj, this.sucursalLngobj),

          travelMode: google.maps.TravelMode.DRIVING,
          avoidTolls: true
        },
        (response, status) => {
          if (status === google.maps.DirectionsStatus.OK) {
            this.directionsDisplay.setDirections(response);
            const km = response.routes[0].legs[0].distance.value / 1000;
            console.log(km);
            if (km >= 0 && km <= 7) {
              this.cargoDomicilio = 4;
            } else if (km >= 8 && km <= 8.99999) {
              this.cargoDomicilio = 4.5;
            } else if (km >= 9 && km <= 9.99999) {
              this.cargoDomicilio = 5;
            } else if (km >= 10 && km <= 10.99999) {
              this.cargoDomicilio = 5.5;
            } else if (km >= 11 && km <= 11.99999) {
              this.cargoDomicilio = 6;
            } else if (km >= 12 && km <= 12.99999) {
              this.cargoDomicilio = 6.5;
            } else if (km >= 13 && km <= 13.99999) {
              this.cargoDomicilio = 7;
            } else if (km >= 14 && km <= 14.99999) {
              this.cargoDomicilio = 7.5;
            } else if (km >= 15 && km <= 15.99999) {
              this.cargoDomicilio = 8;
            } else if (km >= 16 && km <= 16.99999) {
              this.cargoDomicilio = 8.5;
            } else if (km >= 17 && km <= 17.99999) {
              this.cargoDomicilio = 9;
            } else if (km >= 18 && km <= 18.99999) {
              this.cargoDomicilio = 9.5;
            } else if (km >= 19 && km <= 19.99999) {
              this.cargoDomicilio = 10;
            } else if (km >= 20 && km <= 20.99999) {
              this.cargoDomicilio = 10.5;
            } else if (km >= 21 && km <= 21.99999) {
              this.cargoDomicilio = 11;
            } else if (km >= 22 && km <= 22.99999) {
              this.cargoDomicilio = 11.5;
            } else if (km >= 23 && km <= 23.99999) {
              this.cargoDomicilio = 12;
            } else if (km >= 24 && km <= 24.99999) {
              this.cargoDomicilio = 12.5;
            } else if (km >= 25 && km <= 25.99999) {
              this.cargoDomicilio = 13;
            } else if (km >= 26 && km <= 26.99999) {
              this.cargoDomicilio = 13.5;
            } else if (km >= 27 && km <= 27.99999) {
              this.cargoDomicilio = 14;
            } else if (km >= 28 && km <= 28.99999) {
              this.cargoDomicilio = 14.5;
            } else if (km >= 29 && km <= 29.99999) {
              this.cargoDomicilio = 15;
            } else if (km >= 30 && km <= 30.99999) {
              this.cargoDomicilio = 15.5;
            } else if (km >= 31 && km <= 31.99999) {
              this.cargoDomicilio = 16;
            } else if (km >= 32 && km <= 32.99999) {
              this.cargoDomicilio = 16.5;
            } else if (km >= 33 && km <= 33.99999) {
              this.cargoDomicilio = 17;
            } else if (km >= 34 && km <= 34.99999) {
              this.cargoDomicilio = 17.5;
            } else if (km >= 35 && km <= 35.99999) {
              this.cargoDomicilio = 18;
            } else if (km >= 36) {
              console.log('no aplica');
              this.cargoDomicilio = 0;
              this.alertService.presentToast(
                'La distancia de tu ubicacion  no esta contemplada en nuestro rango de servicio'
              );
            }
          } else {
            alert('Could not display directions due to: ' + status);
          }
        },
        (error) => {
          console.log(error);
          alert(JSON.stringify(error));
        }
      );
    });
  }

  async confirmarPedEfecNoFactura() {
    console.log('estado de factura', this.factura);
    console.log('estado de pago', this.selecion);

    if (this.autocomplete.input === ' ' || this.autocomplete.input === undefined) {
      this.alertService.presentToast('Por favor rellenar todos los campos');

      return;
    }

    const alert = await this.alert.create({
      header: 'Aviso',
      message: 'Su forma de pago es correcta? ',
      buttons: [
        {
          text: 'Cambiar',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Confirmar',
          handler: () => {

            this.storage.get('mapsLocation').then((val) => {

              const orderLat = val.lat;
              const orderLng = val.lng;
              const platillos = {};
              for (const element of this.listPedido) {
                platillos[element.id] = {
                  cantidad: element.cantidad
                };
              }
              this.order
                .realizarOrden({
                  usuario_id: this.user.id,
                  whatsapp: this.user.whatsapp,
                  telefonofac: this.user.telefono,
                  direccion: this.autocomplete.input,
                  link: this.subTotal2,
                  total: this.subTotal2 + this.cargoDomicilio,
                  platillos,
                  formapago: this.selecion,
                  observaciones: this.observaciones,
                  factura: this.factura,
                  cargoDomicilio: this.cargoDomicilio,
                  sucursal_id: this.sucursalOrden.id,
                  pagoespecial: 0,
                  lat: orderLat,
                  lng: orderLng
                })
                .subscribe(
                  (resp) => {
                    if (resp.sucess) {
                      this.storage.set('pedido', null).then(() => {
                        this.alertService.presentToast(
                          'Hemos recibido tu orden te notificaremos cuando sea asignada a un repartidor!!'
                        );
                        this.router.navigate(['menu/orderhistorydetail/:order', resp.orden]);
                      });
                    } else {
                      this.alertService.presentToast('Ha ocurrido un error');
                    }
                  },
                  (error) => {
                    this.alertService.presentToast('Ha ocurrido un error');
                  }
                );
            });

          }
        }
      ]
    });
    await alert.present();
  }


  async confirmarPedEfecSiFactura() {
    console.log('estado de factura', this.factura);
    console.log('estado de pago', this.selecion);

    if (this.autocomplete.input === ' ' || this.autocomplete.input === undefined) {
      this.alertService.presentToast('Por favor rellenar todos los campos');

      return;
    }

    const alert = await this.alert.create({
      header: 'Aviso',
      message: 'Su forma de pago es correcta? ',
      buttons: [
        {
          text: 'Cambiar',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Confirmar',
          handler: () => {
            this.storage.get('mapsLocation').then((val) => {

              const orderLat = val.lat;
              const orderLng = val.lng;
              const platillos = {};

              for (const element of this.listPedido) {
                platillos[element.id] = {
                  cantidad: element.cantidad
                };
              }
              this.order
                .realizarOrden({
                  usuario_id: this.user.id,
                  whatsapp: this.user.whatsapp,
                  telefonofac: this.user.telefono,
                  nombrefac: this.data.nombrefac,
                  rucfac: this.data.rucfac,
                  emailfac: this.data.emailfac,
                  direccionfac: this.data.direccionfac,
                  direccion: this.autocomplete.input,
                  link: this.subTotal2,
                  total: this.subTotal2 + this.subTotal2 * this.iva / 100 + this.cargoDomicilio,
                  platillos: platillos,
                  formapago: this.selecion,
                  observaciones: this.observaciones,
                  factura: this.factura,
                  iva: this.subTotal2 * this.iva / 100,
                  cargoDomicilio: this.cargoDomicilio,
                  sucursal_id: this.sucursalOrden.id,
                  pagoespecial: 0,
                  lat: orderLat,
                  lng: orderLng

                })
                .subscribe(
                  (resp) => {
                    if (resp.sucess) {
                      this.storage.set('pedido', null).then(() => {
                        this.alertService.presentToast(
                          'Hemos recibido tu orden te notificaremos cuando sea asignada a un repartidor!!'
                        );
                        this.router.navigate(['menu/orderhistorydetail/:order', resp.orden]);
                        console.log(resp.orden);
                      });
                    } else {
                      this.alertService.presentToast('Ha ocurrido un error');
                    }
                  },
                  (error) => {
                    this.alertService.presentToast('Ha ocurrido un error');
                  }
                );
            });

          }
        }
      ]
    });
    await alert.present();

  }

  async confirmarPedPaypNoFactura() {

    console.log('estado de factura', this.factura);
    console.log('estado de pago', this.selecion);

    if (this.autocomplete.input === ' ' || this.autocomplete.input === undefined) {
      this.alertService.presentToast('Por favor rellenar todos los campos');

      return;
    }

    const alert = await this.alert.create({
      header: 'Aviso',
      message: 'Su forma de pago es correcta? ',
      buttons: [
        {
          text: 'Cambiar',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Confirmar',
          handler: () => {

            this.storage.get('mapsLocation').then((val) => {

              const orderLat = val.lat;
              const orderLng = val.lng;
              const platillos = {};
              for (const element of this.listPedido) {
                platillos[element.id] = {
                  cantidad: element.cantidad
                };
              }

              this.order
                .realizarOrden({
                  usuario_id: this.user.id,
                  whatsapp: this.user.whatsapp,
                  telefonofac: this.user.telefono,
                  direccion: this.autocomplete.input,
                  sucursal_id: this.sucursalOrden.id,
                  total:
                    this.subTotal2 +
                    this.cargoDomicilio +
                    (this.subTotal2 + this.cargoDomicilio) * this.cargoPaypal / 100,
                  platillos: platillos,
                  formapago: this.selecion,
                  observaciones: this.observaciones,
                  factura: this.factura,
                  cargoDomicilio: this.cargoDomicilio,
                  lat: orderLat,
                  lng: orderLng,
                  pagoespecial: (this.subTotal2 + this.cargoDomicilio) * this.cargoPaypal / 100
                })
                .subscribe(
                  (resp) => {
                    if (resp.sucess) {
                      this.storage.set('pedido', null).then(() => {
                        this.alertService.presentToast(
                          'Hemos recibido tu orden te notificaremos cuando sea asignada a un repartidor!!'
                        );
                        this.router.navigate(['menu/orderhistorydetail/:order', resp.orden]);
                      });
                    } else {
                      this.alertService.presentToast('Ha ocurrido un error');
                    }
                  },
                  (error) => {
                    this.alertService.presentToast('Ha ocurrido un error');
                  }
                );
            });
          }
        }
      ]
    });
    await alert.present();
  }

  async confirmarPedPaypSiFactura() {

    console.log('estado de factura', this.factura);
    console.log('estado de pago', this.selecion);

    if (this.autocomplete.input === ' ' || this.autocomplete.input === undefined) {
      this.alertService.presentToast('Por favor rellenar todos los campos');

      return;
    }

    const alert = await this.alert.create({
      header: 'Aviso',
      message: 'Su forma de pago es correcta? ',
      buttons: [
        {
          text: 'Cambiar',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Confirmar',
          handler: () => {

            this.storage.get('mapsLocation').then((val) => {

              const orderLat = val.lat;
              const orderLng = val.lng;

              const platillos = {};
              for (const element of this.listPedido) {
                platillos[element.id] = {
                  cantidad: element.cantidad
                };
              }

              this.order
                .realizarOrden({
                  usuario_id: this.user.id,
                  whatsapp: this.user.whatsapp,
                  telefonofac: this.user.telefono,
                  nombrefac: this.data.nombrefac,
                  rucfac: this.data.rucfac,
                  emailfac: this.data.emailfac,
                  direccionfac: this.data.direccionfac,
                  direccion: this.autocomplete.input,
                  sucursal_id: this.sucursalOrden.id,
                  total:
                    this.subTotal2 +
                    this.cargoDomicilio +
                    this.subTotal2 * this.iva / 100 +
                    (this.subTotal2 + this.subTotal2 * this.iva / 100 + this.cargoDomicilio) *
                    this.cargoPaypal /
                    100,
                  platillos: platillos,
                  formapago: this.selecion,
                  observaciones: this.observaciones,
                  factura: this.factura,
                  iva: this.subTotal2 * this.iva / 100,
                  cargoDomicilio: this.cargoDomicilio,
                  lat: orderLat,
                  lng: orderLng,
                  pagoespecial:
                    (this.subTotal2 + this.subTotal2 * this.iva / 100 + this.cargoDomicilio) *
                    this.cargoPaypal /
                    100
                })
                .subscribe(
                  (resp) => {
                    if (resp.sucess) {
                      this.storage.set('pedido', null).then(() => {
                        this.alertService.presentToast(
                          'Hemos recibido tu orden te notificaremos cuando sea asignada a un repartidor!!'
                        );
                        this.router.navigate(['menu/orderhistorydetail/:order', resp.orden]);
                      });
                    } else {
                      this.alertService.presentToast('Ha ocurrido un error');
                    }
                  },
                  (error) => {
                    this.alertService.presentToast('Ha ocurrido un error');
                  }
                );
            });
          }
        }
      ]
    });
    await alert.present();
  }


  payPaypal() {
    console.log('Pay ????');
    if (this.seleccion === 1 && this.factura === true) {
      this.totalPaypalFactura = this.subTotal2 +
        this.cargoDomicilio +
        this.subTotal2 * this.iva / 100 +
        (this.subTotal2 + this.subTotal2 * this.iva / 100 + this.cargoDomicilio) *
        this.cargoPaypal /
        100;

      this.payPal
        .init({
          PayPalEnvironmentProduction: 'AWsWCqXwgNPc85k0pLg6w2CqvOkcR6PEoD8oD2y3MCN5f1xkfkAFyAVGIvr0DAaIGJvZvHwBJzc7x-7l',
          PayPalEnvironmentSandbox:
            '',
        })
        .then(() => {
          this.payPal
            .prepareToRender(
              'PayPalEnvironmentSandbox',
              new PayPalConfiguration({
                acceptCreditCards: true,
                languageOrLocale: 'es-ES',
                merchantName: 'SushiYa',
                merchantPrivacyPolicyURL: '',
                merchantUserAgreementURL: ''
              })
            )
            .then(() => {
              const payment = new PayPalPayment(this.totalPaypalFactura, 'USD', 'SushiYA', 'sale');
              this.payPal.renderSinglePaymentUI(payment).then(
                (response) => {
                  console.log('Pago Efectuado ', response);
                  alert(response);
                },
                () => {
                  console.log('Algun error ha ocurrido');
                }
              );
            });
        });

    } else {

      this.totalPaypalFactura = this.subTotal2 +
        this.cargoDomicilio +
        (this.subTotal2 + this.cargoDomicilio) * this.cargoPaypal / 100;

      this.payPal
        .init({
          PayPalEnvironmentProduction: 'AWsWCqXwgNPc85k0pLg6w2CqvOkcR6PEoD8oD2y3MCN5f1xkfkAFyAVGIvr0DAaIGJvZvHwBJzc7x-7l',
          PayPalEnvironmentSandbox:
            '',
        })
        .then(() => {
          this.payPal
            .prepareToRender(
              'PayPalEnvironmentSandbox',
              new PayPalConfiguration({
                acceptCreditCards: true,
                languageOrLocale: 'es-ES',
                merchantName: 'SushiYa',
                merchantPrivacyPolicyURL: '',
                merchantUserAgreementURL: ''
              })
            )
            .then(() => {
              const payment = new PayPalPayment(this.totalPaypalFactura, 'USD', 'SushiYA', 'sale');
              this.payPal.renderSinglePaymentUI(payment).then(
                (response) => {
                  console.log('Pago Efectuado ', response);
                  alert(response);
                },
                () => {
                  console.log('Algun error ha ocurrido');
                }
              );
            });
        });
    }

  }

}
