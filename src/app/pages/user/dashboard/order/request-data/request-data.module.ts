import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { RequestDataPage } from './request-data.page';
import { ModalFacturaPageModule } from 'src/app/pages/global/modal-factura/modal-factura.module';

import { ModalPaypalPageModule } from 'src/app/pages/global/modal-paypal/modal-paypal.module';
import { PaypalCheckboxPageModule } from 'src/app/pages/global/paypal-checkbox/paypal-checkbox.module';
import { ModalFacturaPage } from 'src/app/pages/global/modal-factura/modal-factura.page';
import { ModalPaypalPage } from 'src/app/pages/global/modal-paypal/modal-paypal.page';
import { PaypalCheckboxPage } from 'src/app/pages/global/paypal-checkbox/paypal-checkbox.page';

const routes: Routes = [
  {
    path: '',
    component: RequestDataPage
  }
];

@NgModule({
  /*  entryComponents: [
     ModalFacturaPage,
     ModalPaypalPage,
     PaypalCheckboxPage], */
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    /*  ModalFacturaPageModule,
     ModalPaypalPageModule,
     PaypalCheckboxPageModule */
  ],
  declarations: [RequestDataPage]
})
export class RequestDataPageModule { }
