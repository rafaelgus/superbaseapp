import { Component, OnInit } from '@angular/core';
import { GlobalService } from 'src/app/services/global/global.service';
import { Router, ActivatedRoute, RouterEvent, NavigationStart } from '@angular/router';
import { AlertService } from 'src/app/services/util/alert.service';
import { Storage } from '@ionic/storage';


@Component({
  selector: 'app-sub-category',
  templateUrl: './sub-category.page.html',
  styleUrls: ['./sub-category.page.scss'],
})
export class SubCategoryPage implements OnInit {

  titulo = 'SubCategorias de Productos';
  subcategorias: any[];
  skip = 0;
  platillos: any = [];
  listPedido: any = [];
  id: any;
  subid: any;
  currentPage: string;
  navigationSubscription;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private storage: Storage,
    private alertService: AlertService,
    private global: GlobalService
  ) { }

  ngOnInit() {
    this.subid = this.route.snapshot.paramMap.get('id');
    this.getpedido();

    this.global.subcategoria(this.subid).subscribe((resp) => {
      console.log('subCategorias:', resp);
      this.subcategorias = resp.subcategorias;
    });

  }

  ionViewDidEnter() {

    this.getpedido();
  }

  async getpedido() {
    this.storage.get('pedido').then((resp) => {
      if (resp != null) {
        this.listPedido = resp;
      }
    });
  }
  pedido(listPedido) {
		/* this.navCtrl.push('DetallePedidoPage', {
			listPedido: this.listPedido
		}); */
    this.router.navigate(['menu/reqsdetails/']);
  }

  itemSelected(subcat) {
    this.id = subcat.id_categoria;
    this.subid = subcat.id;
    this.router.navigate(['menu/catdetalle/', this.id, this.subid]);
  }

  backButton() {
    this.router.navigate(['menu/categorias']);
  }

  doInfinite(infiniteScroll) {
    this.skip += 15;
    this.global.platillos(this.skip).subscribe((data) => {
      const items = data.response.platillos;
      if (items !== undefined) {
        for (const item of items) {
          this.platillos.push(item);
        }
      } else {
        this.alertService.presentToast('No hay mas productos para mostrar');
      }
      infiniteScroll.target.complete();
    });
  }


}
