import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.page.html',
  styleUrls: ['./landing.page.scss'],
})
export class LandingPage implements OnInit {

  titulo = 'DemoApp';
  listPedido: any = [];

  constructor(private storage: Storage) { }

  ngOnInit() {
    this.getpedido();
  }

  ionViewDidEnter() {
    console.log('regresando');
    this.getpedido();
  }

  async getpedido() {
    this.storage.get('pedido').then((resp) => {
      if (resp != null) {
        this.listPedido = resp;
      }
    });
  }
}
