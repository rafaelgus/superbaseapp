import { Component, OnInit } from '@angular/core';

import { GlobalService } from 'src/app/services/global/global.service';



@Component({
  selector: 'app-faq',
  templateUrl: './faq.page.html',
  styleUrls: ['./faq.page.scss'],
})
export class FaqPage implements OnInit {

  titulo = 'preguntas frecuentes';

  faq: any = [];
  soporte: any = [];

  constructor(
    public global: GlobalService,

  ) { }

  ngOnInit() {

    this.global.getfaq().subscribe((resp) => {

      this.faq = resp;
      console.log(this.faq);

    });
  }

}
