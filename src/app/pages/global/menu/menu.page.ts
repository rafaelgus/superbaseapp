import { Component, OnInit, AfterContentInit } from '@angular/core';
import { Router, RouterEvent } from '@angular/router';
import { Storage } from '@ionic/storage';
import { AuthService } from 'src/app/services/auth/auth.service';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { NativeGeocoder, NativeGeocoderResult, NativeGeocoderOptions } from '@ionic-native/native-geocoder/ngx';
import { Platform } from '@ionic/angular';
import { GlobalService } from 'src/app/services/global/global.service';
import { CallNumber } from '@ionic-native/call-number/ngx';
import {
  GoogleMaps,
  GoogleMap,
  GoogleMapsEvent,
  LatLng,
  CameraPosition,
  MarkerOptions,
  GeocoderRequest,
  GeocoderResult,
  MyLocation,
  Marker,
  GoogleMapsAnimation,
  Geocoder,
  LocationService,
} from '@ionic-native/google-maps';

declare var google;
declare var geocoder;
@Component({
  selector: 'app-menu',
  templateUrl: './menu.page.html',
  styleUrls: ['./menu.page.scss'],
})
export class MenuPage implements OnInit, AfterContentInit {

  title = 'SushiYA';

  private isLoging;
  private user;
  watch: any;
  geocoder: Geocoder;

  geoLatitude: number;
  geoLongitude: number;
  geoAccuracy: number;
  geoAddress: string;

  watchLocationUpdates: any;
  loading: any;
  isWatching: boolean;

  latitude: any;
  longitude: any;
  latLng: any = {};
  map: GoogleMap;
  address: string;

  config: any = [];


  appPages = [
    {
      title: 'Inicio',
      url: '/menu/landing',
      icon: 'home'
    },
    {
      title: 'Nueva Orden',
      url: '/menu/categorias',
      icon: 'md-clipboard'
    },
    {
      title: 'Historia de Ordenes',
      url: '/menu/orderhistory',
      icon: 'ios-archive'
    },

    {
      title: 'Rastrear orden',
      url: '/menu/trackorderlist',
      icon: 'locate'
    },
    {
      title: 'Cuenta',
      url: '/menu/userconfig',
      icon: 'ios-person'
    },
    {
      title: 'Preguntas Frequentes',
      url: '/menu/faq',
      icon: 'help-circle-outline'
    },

  ];

  selectedPath = '';

  constructor(
    private router: Router,
    private storage: Storage,
    private auth: AuthService,
    private geolocation: Geolocation,
    private nativeGeocoder: NativeGeocoder,
    private platform: Platform,
    private global: GlobalService,
    private callNumber: CallNumber
  ) {

    this.geocoder = new google.maps.Geocoder();
    this.router.events.subscribe((event: RouterEvent) => {
      if (event && event.url) {
        this.selectedPath = event.url;
      }
    });
  }

  ngOnInit() {

    this.platform.ready();

    this.storage.get('isLoging').then((resp) => {
      this.isLoging = resp;
      this.storage.get('user').then((data) => {
        this.user = data;
        const id = this.user.id;
        this.enviarUbicacion(id);
      });
    });
    this.pruebaGeocode();

    this.global.getconfig().subscribe((resp) => {
      this.config = resp;

    });
  }

  ngAfterContentInit() {

    /*   this.storage.get('user').then((val) => {
        this.user = val;
        this.enviarUbicacion();
      }); */



  }
  exit() {
    this.storage.set('user', undefined).then(() => {
      this.storage.clear();
      this.router.navigate(['login']);
    });
    navigator['app'].exitApp();
  }

  callSupport() {
    this.callNumber.callNumber(this.config[4].valor, true)
      .then(res => console.log('Llamando ', res))
      .catch(err => console.log('Ha ocurrido un error', err));

  }

  enviarUbicacion(id) {
    const watch = this.geolocation.watchPosition();
    watch.subscribe((data) => {
      console.log(data);
      const coords = { lat: data.coords.latitude, lng: data.coords.longitude };
      this.storage.set('position', coords);
      this.auth.enviarMiPosicion(id, coords);
      /* this.auth.enviarMiPosicion(this.user.id, coords) */
      this.pruebaGeocode();
      this.pruebaNativeGeocode();
    });

  }


  pruebaNativeGeocode() {
    LocationService.getMyLocation().then((location: MyLocation) => {
      console.log(JSON.stringify(location, null, 2));
      const latLng = location.latLng;
      this.storage.set('mapsLocation', location.latLng);

      let options: GeocoderRequest = {
        position: latLng
      };

      // Latitude, longitude -> address
      Geocoder.geocode({
        position: latLng
      }).then((results: GeocoderResult[]) => {
        if (results.length === 0) {
          // Not found
          return null;
        }

        const address: any = [
          results[0].extra.lines[0] || '',
          results[0].extra.lines[1] || '',
          results[0].extra.lines[2] || '',
          results[0].extra.lines[3] || '',
          results[0].extra.lines[4] || '',
          results[0].extra.lines[5] || '',
          results[0].extra.lines[6] || '',
        ].join(', ');

        /*  const address: any = [
           results[0].subThoroughfare || '',
           results[0].thoroughfare || '',
           results[0].locality || '',
           results[0].adminArea || '',
           results[0].postalCode || '',
           results[0].country || ''].join(', '); */

        console.log(address);
        console.log(results);

        this.storage.set('mapsAddress', address);



      });


    });
  }

  pruebaGeocode() {

    const options: NativeGeocoderOptions = {
      useLocale: true,
      maxResults: 2
    };

    this.geolocation.getCurrentPosition().then((resp) => {
      const lat = resp.coords.latitude;
      const lng = resp.coords.longitude;
      const latLng = { lat, lng };
      console.log('getcurrent:', latLng);

      this.nativeGeocoder.reverseGeocode(lat, lng, options)
        .then((result: NativeGeocoderResult[]) => {
          console.log(JSON.stringify(result[0]));
          const address = result[0];
          this.storage.set('direccion', address);
        })
        .catch((error: any) => console.log(error));

      /* this.geocoder.geocode({ 'location': latLng }, (results, status) => {

        if (status === 'OK') {

          const address = results[0].formatted_address;
          this.storage.set('direccion', address);


        }
      }); */
    }).catch((error) => {
      console.log('Error getting location', error);
    });

  }


}
