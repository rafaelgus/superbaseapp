import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import Pusher from 'pusher-js';

/* declare const Pusher: any; */
declare const id_cadete: any;

@Injectable({
	providedIn: 'root'
})
export class PusherService {
	id_cadete: any;
	constructor(public http: HttpClient) {
		const pusher = new Pusher('4665f01ss56ed3e47df7', {
			cluster: 'us2',
			encrypted: true
		});
		this.channel = pusher.subscribe(`orden-asignada.${id_cadete}`);
	}
	channel;
	public init() {
		return this.channel;
	}
}
