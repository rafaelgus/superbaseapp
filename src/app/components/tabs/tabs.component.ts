import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { DataService } from 'src/app/services/data/data.service';
import { Tabs } from 'src/app/interfaces/interfaces';

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.component.html',
  styleUrls: ['./tabs.component.scss'],
})
export class TabsComponent implements OnInit {

  tabsOpts: Observable<Tabs[]>;

  trackorder = 'trackorder';



  constructor(private dataService: DataService) { }

  ngOnInit() {
    this.tabsOpts = this.dataService.getTabsOpts();
  }
}
