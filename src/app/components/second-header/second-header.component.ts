import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-second-header',
  templateUrl: './second-header.component.html',
  styleUrls: ['./second-header.component.scss'],
})
export class SecondHeaderComponent implements OnInit {
  @Input() titulo: string;

  constructor() { }

  ngOnInit() { }

}
